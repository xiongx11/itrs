#! /bin/bash

for file in /bin/*

do
    cd /bin
    now_t=$(date +%s)
    lastupdate=$(stat -c %Y "${file##*/}")
    number=$(expr $now_t - $lastupdate)
    echo $number
done
